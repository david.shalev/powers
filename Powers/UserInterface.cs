﻿using System;
using System.Collections.Generic;

namespace Powers
{
    internal class UserInterface
    {

        public static int getIntOnRangeFromUser(int min, int max)
        {
            while (true)
            {
                bool IsNumber = Int32.TryParse(Console.ReadLine(), out int chosenNumber);
                if (!IsNumber)
                {
                    Console.WriteLine("the given input is not a number,");
                    Console.WriteLine("please type again");
                    continue;
                }

                if (chosenNumber < 2 || chosenNumber > 4)
                {
                    Console.WriteLine("the given number is not in the range of {0} to {1}",min,max);
                    Console.WriteLine("please type again");
                    continue;
                }

                return chosenNumber;
            }
        }

        public static int getIntDividedByFromUser(int divide)
        {
            while (true)
            {
                bool isNumber = Int32.TryParse(Console.ReadLine(), out int chosenNum);
                if (!isNumber)
                {
                    Console.WriteLine("the given input is not a number");
                    continue;
                }

                if (chosenNum % divide != 0)
                {
                    Console.WriteLine("the given number is not divided by three!!");
                    continue;
                }

                return chosenNum;
            }
        }

        internal static string getStringFromUser()
        {
            string chosenStringByUser = Console.ReadLine();
            return chosenStringByUser;
        }

        
    }
}
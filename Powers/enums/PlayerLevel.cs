﻿namespace Powers
{
    public enum PlayerLevel
    {
        High,
        Medium,
        Low
    }
}
﻿using System;
using System.Collections.Generic;

namespace Powers
{
    public class Powers
    {
        private const int minNumOfTeams = 2;
        private const int maxNumOfTeams = 4;
        

        private static Random random;

        static Powers()
        {
            random = new Random();
        }

        public static void Start()
        {
            Console.WriteLine("Welcome to powers app!!!");
            Console.WriteLine("please type number of teams between {0} to {1}",minNumOfTeams,maxNumOfTeams);
            int numOfTeams = UserInterface.getIntOnRangeFromUser(minNumOfTeams, maxNumOfTeams);
            Console.WriteLine("please type number(divided by three) of players in each team");
            int numOfPlayersPerTeam = UserInterface.getIntDividedByFromUser(3);
            int totalPlayers = numOfTeams * numOfPlayersPerTeam;
            int numOfPlayersPerLevel = totalPlayers/3;
            Console.Clear();
            Console.WriteLine("you should give the details of {0} players", totalPlayers);
            Console.WriteLine("First,type the names of {0} low level players",numOfPlayersPerLevel);
            List<Player> lowLevelPlayers = getAllPlayersByLevel(numOfPlayersPerLevel,PlayerLevel.Low);
            Console.Clear();
            Console.WriteLine("Now,type the names of {0} medium level players", numOfPlayersPerLevel);
            List<Player> mediumLevelPlayers = getAllPlayersByLevel(numOfPlayersPerLevel, PlayerLevel.Medium);
            Console.Clear();
            Console.WriteLine("Now,type the names of {0} high level players", numOfPlayersPerLevel);
            List<Player> highLevelPlayers = getAllPlayersByLevel(numOfPlayersPerLevel, PlayerLevel.High);
            List<List<Player>> allTeams=MakeRandomPowers(lowLevelPlayers, mediumLevelPlayers, highLevelPlayers,numOfTeams,numOfPlayersPerTeam);
            PrintTeams(allTeams);

        }

        private static void PrintTeams(List<List<Player>> allTeams)
        {
            for (int i = 0; i < allTeams.Count; i++)
            {
                Console.WriteLine("players of team number {0}:",i+1);
                
                foreach (Player player in allTeams[i])
                {
                    Console.WriteLine(player.ToString());
                }
            }

            Console.ReadLine();
        }

        private static List<List<Player>> MakeRandomPowers(List<Player> lowLevelPlayers,
            List<Player> mediumLevelPlayers, List<Player> highLevelPlayers, int numOfTeams,
            int numOfPlayersPerTeam)
        {
            List<List<Player>> allTeams = new List<List<Player>>();
            for (int i = 0; i < numOfTeams; i++)
            {
                List<Player> team = CreateTeam(lowLevelPlayers, mediumLevelPlayers, highLevelPlayers, numOfPlayersPerTeam);
                allTeams.Add(team);
            }

            return allTeams;
        }

        private static List<Player> CreateTeam(List<Player> lowLevelPlayers,
            List<Player> mediumLevelPlayers, List<Player> highLevelPlayers, int numOfPlayersPerTeam)
        {
            List<Player> team = new List<Player>(); 
            int numOfPlayersPerLevelInTeam = numOfPlayersPerTeam / 3;
            for (int i = 0; i < numOfPlayersPerLevelInTeam; i++)
            {
                Player lowLevelPlayer = getRandomPlayerByLevel(lowLevelPlayers);
               
                team.Add(lowLevelPlayer);
                
                lowLevelPlayers.Remove(lowLevelPlayer);
            }
            for (int i = 0; i < numOfPlayersPerLevelInTeam; i++)
            {
                Player mediumLevelPlayer = getRandomPlayerByLevel(mediumLevelPlayers);
                team.Add(mediumLevelPlayer);
                mediumLevelPlayers.Remove(mediumLevelPlayer);
                
            }
            for (int i = 0; i < numOfPlayersPerLevelInTeam; i++)
            {
                Player highLevelPlayer = getRandomPlayerByLevel(highLevelPlayers);
                team.Add(highLevelPlayer);
                highLevelPlayers.Remove(highLevelPlayer);
            }

            return team;
        }

        private static Player getRandomPlayerByLevel(List<Player> PlayersByLevel)
        {
            
            int randomNum = random.Next(PlayersByLevel.Count);
            return PlayersByLevel[randomNum];
        }

        private static List<Player> getAllPlayersByLevel(int numOfPlayersPerLevel,PlayerLevel playerLevel)
        {
            List<Player> Players =new List<Player>();
            for (int i = 0; i < numOfPlayersPerLevel; i++)
            {
                Console.WriteLine("please type {0} level player number {1}",playerLevel,i+1);
                string playerName = UserInterface.getStringFromUser();
                Player newPlayer =new Player(playerName,playerLevel);
                Players.Add(newPlayer);
            }

            return Players;
        }

       
    }
}
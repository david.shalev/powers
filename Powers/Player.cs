﻿using System;

namespace Powers
{
    public class Player
    {
        private string name;
        private PlayerLevel level;

        public Player(string name, PlayerLevel level)
        {
            this.name = name;
            this.level = level;
        }

        public override string ToString()
        {
            return "Name:" +this.name + "  Level:" +this.level;
        }
    }
}